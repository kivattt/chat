#include <iostream>
#include <string>
#include <sstream>
#include <SFML/Network.hpp>

using std::string;

void usage(){
	std::cout << "Usage: send [IP address] [port]\n";
}

string encrypt(const string str){
	string out="";
	for (auto chr : str)
		out += chr^175;
	return out;
}

int main(int argc, char *argv[]){
	if (argc < 3){
		usage();
		return 1;
	}

	sf::TcpSocket socket;

	sf::IpAddress ipAddr(argv[1]);

	unsigned short port;
	try{
		port = std::stoi(argv[2]);
	} catch(const std::invalid_argument&){
		usage();
		return 2;
	}

	// Attempt connecting till connected
	while (socket.connect(ipAddr, port, sf::seconds(1)) != sf::Socket::Done){}

	std::cout << "Connected to " << ipAddr.toString() << " on port " << port << ".\n";
	std::cout << "/help for help.\n";

	bool exit=false;
	while (!exit){
		string input;
		std::getline(std::cin, input);
		if (input.empty()) continue;

		if (input == "/help"){
			std::cout << "Commands:\n";
			std::cout << "\t/help\tDisplay this help page\n";
			std::cout << "\t/q   \tQuit\n";
		} else if (input == "/q"){
			exit=true;
		} else {
			socket.send(encrypt(input).c_str(), input.size() + 1);
		}
	}

	std::cout << "Disconnecting...\n";
	socket.disconnect();
}
