#include <iostream>
#include <string>
#include <SFML/Network.hpp>

using std::string;

void usage(){
	std::cout << "Usage: receive [IP address] [port]\n";
}

string decrypt(const string str){
	string out="";
	for (auto chr : str)
		out += chr^175;
	return out;
}

int main(int argc, char *argv[]){
	if (argc < 3){
		usage();
		return 1;
	}

	sf::TcpListener listener;

	const sf::IpAddress ipAddr(argv[1]);

	unsigned port;
	try{
		port = std::stoi(argv[2]);
	} catch (const std::invalid_argument&){
		usage();
		return 2;
	}

	// Attempt listening until listening
	if (listener.listen(port) != sf::Socket::Done){
		std::cerr << "Unable to listen on port " << port << ".\n";
		return 3;
	}
	std::cout << "Listening on port " << port << ".\n";

	sf::TcpSocket socket;

	// Attempt accepting until accepted
	while (listener.accept(socket) != sf::Socket::Done){}
	std::cout << "Accepted " << ipAddr.toString() << ".\n";

	for (;;){
		char msgBuffer[512];
		std::size_t recieved = 0;
		socket.receive(msgBuffer, sizeof(msgBuffer), recieved);
		std::cout << decrypt(msgBuffer) << '\n';
	}
}
